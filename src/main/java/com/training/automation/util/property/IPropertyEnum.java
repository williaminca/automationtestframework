package com.training.automation.util.property;

/**
 * @author william.guo
 */
public interface IPropertyEnum {
    public String getKeyText();

    public String getDescription();
}
