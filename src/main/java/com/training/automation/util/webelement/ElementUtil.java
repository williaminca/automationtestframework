package com.training.automation.util.webelement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ElementUtil {

    public static void waitForElementVisible(WebDriver driver, WebElement element){
        waitForElementVisible(driver, element, 10);
    }

    public static void waitForElementVisible(WebDriver driver, WebElement element, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementDisappear(WebDriver driver, WebElement element, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public static void inputTextField(WebElement textField, String text){
        textField.clear();
        textField.sendKeys(text);
    }

    public static void waitForElementToBeClickable(WebDriver driver, WebElement element, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitForElementToBeClickable(WebDriver driver, WebElement element){
        waitForElementToBeClickable(driver, element, 10);
    }
}
