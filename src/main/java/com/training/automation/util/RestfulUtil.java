package com.training.automation.util;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

/**
 * William Guo
 */
public final class RestfulUtil {

    public static Response sendRequest(String authenticationKey, String seaviceURL, String request){

        return given().
                auth().
                basic(authenticationKey, "").
                contentType(ContentType.JSON).body(request).
                expect().
                when().
                post(seaviceURL).
                then().
                extract().
                response();
    }
}
