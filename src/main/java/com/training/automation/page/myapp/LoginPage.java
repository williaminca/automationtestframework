package com.training.automation.page.myapp;

import com.training.automation.page.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author william.guo
 */
public class LoginPage extends BasePage {
    //Page elements
    @FindBy(id = "username")
    private WebElement txtUserName;

    @FindBy(id = "password")
    private WebElement txtPassword;

    @FindBy(name = "login")
    private WebElement signButton;

    public LoginPage(WebDriver driver){
        super(driver);
    }

    public WorkspacePage login(String userName, String password) {
        txtUserName.clear();
        txtUserName.sendKeys(userName);
        txtPassword.clear();
        txtPassword.sendKeys(password);

        signButton.click();

        return new WorkspacePage(driver);
    }
}
