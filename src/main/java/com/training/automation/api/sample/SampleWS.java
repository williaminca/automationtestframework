package com.training.automation.api.sample;

import com.training.automation.util.RestfulUtil;
import com.training.automation.util.property.AutomationProperties;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * all interaction with the Sample web service are implemented here
 * @Author: William Guo
 */
public class SampleWS {
    private static final Logger logger = LoggerFactory.getLogger(SampleWS.class);

    public static Response sendSampleSearchRequest(String message) {
        return RestfulUtil.sendRequest(AutomationProperties.getProperty(SamplePropertyEnum.WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SamplePropertyEnum.SEARCH_URI).trim(),
                message);
    }

    public static Response sendOrderRequest(String message){
        return RestfulUtil.sendRequest(AutomationProperties.getProperty(SamplePropertyEnum.WS_AUTHKEY).trim(),
                AutomationProperties.getProperty(SamplePropertyEnum.PROFILE_REQUEST_URI).trim(),
                message);
    }

}
