package com.training.automation.page.myapp;

import com.training.automation.enumeration.JurisdictionEnum;
import com.training.automation.page.PageTest;
import com.training.automation.util.property.PropertyReader;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class OrderCreateTest extends PageTest {
    private WorkspacePage workspacePage;


    @BeforeMethod
    public void gotoPage(){
        driver.get(PropertyReader.INSTANCE.getValue("BASE_URL"));
        LoginPage loginPage = new LoginPage(this.driver);
        workspacePage = loginPage.login("william", "mypassword");
    }

    @Test
    public void testCreateOrder(){
        OrderPage orderPage = workspacePage.createorder();
        SearchResultPage searchResultPage = orderPage
                .inputReferenceName("fromWilliam01")
                .inputEntityName("abc co")
                .selectJurisdiction(JurisdictionEnum.ONTARIO)
                .clickSubmit();
        SelectOrderPage selectOrderrPage = searchResultPage.clickFirstEligibleResult();
        selectOrderrPage.inputFirstName("QQ")
                .inputLastName("AA")
                .inputEmail("sample@placeholder.com")
                .inputConfirmEmail("sample@placeholder.com")
                .orderCreate();
    }
}
