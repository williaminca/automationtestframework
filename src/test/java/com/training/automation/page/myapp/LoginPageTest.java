package com.training.automation.page.myapp;

import com.training.automation.page.PageTest;
import com.training.automation.util.property.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @Author: William Guo
 */
public class LoginPageTest extends PageTest {
    private LoginPage loginPage;

    @BeforeMethod
    public void gotoPage(){
        driver.get(PropertyReader.INSTANCE.getValue("BASE_URL"));
        loginPage = new LoginPage(this.driver);
    }

    @Test
    public void testLogin() {
        WorkspacePage workspacePage = loginPage.login("william", "mypassword");
        Assert.assertEquals("Sample", driver.getTitle());
    }

}
