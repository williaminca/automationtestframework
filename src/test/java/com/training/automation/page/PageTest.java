package com.training.automation.page;

import com.training.automation.helper.DriverManager;
import com.training.automation.helper.DriverManagerFactory;
import com.training.automation.util.property.AutomationProperties;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * This is the super class that all page tests need to inherent from
 * In this class, the driver is initiated based on the properties file; and login to homepage is done. These two
 * operations can be reused by all other page test
 *
 * @Author: William Guo
 */
public abstract class PageTest {
    protected WebDriver driver;
    protected DriverManager driverManager;
    protected final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @BeforeClass
    public void setupDriver() {
        driverManager = DriverManagerFactory.getManager(AutomationProperties.getProperty(LearningPropertyEnum.BROWSER_TO_USE));
        driver = driverManager.getDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown() {
        driverManager.quitDriver();
    }
}
