package com.training.automation.api.sample;

import com.training.automation.helper.dataprovider.RestfulTestDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class SampleTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @Test(
            dataProvider = "getTestData",
            dataProviderClass = RestfulTestDataProvider.class
    )
    public void test(String requestStr, String expectedResponseStr) {

//
    }
}
