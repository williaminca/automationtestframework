package com.training.automation.helper;

public class DriverManagerFactory {
    public static DriverManager getManager(String browser) {

        DriverManager driverManager;

        switch(browser.toLowerCase()) {
            case "chrome":
                driverManager = new ChromeDriverManager();
                break;

            default:
                driverManager = new ChromeDriverManager();
                break;
        }

        return  driverManager;
    }
}
