package com.training.automation.helper;

public class SampleResponseParser extends JsonParser{
    public SampleResponseParser(String jsonStr) {
        super(jsonStr);
    }

    public String getEntityName(){
        return jsonPath.get("results.entityName[0]");
    }

    public String getResultId(){
        return jsonPath.get("results.resultId[0]").toString();
    }

    public String getOrderId(){
        return jsonPath.get("orderId").toString();
    }

    public String getErrorField(){
        return jsonPath.get("errors.field[0]");
    }

    public String getError(){
        return jsonPath.get("errors.error[0]");
    }

    public String getErrorMsg(){
        return jsonPath.get("errors.message[0]");
    }
}
