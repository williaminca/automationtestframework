package com.training.automation.helper.dataprovider;

import com.training.automation.util.CSVUtil;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

public class RestfulTestDataProvider {

    @DataProvider(name = "getPositiveTestData")
    public static Object[][] getPositiveTestData(Method method){
        return CSVUtil.getTestData(method.getDeclaringClass().getSimpleName() + "_p");
    }

    @DataProvider(name = "getNegativeTestData")
    public static Object[][] getNegativeTestData(Method method){
        return CSVUtil.getTestData(method.getDeclaringClass().getSimpleName() + "_n");
    }

    public static Object[][] getSampleRequestTestData(Method method){
        StringBuffer fileName = new StringBuffer(method.getDeclaringClass().getSimpleName()).append("_profile");
        return CSVUtil.getTestData(fileName.toString());
    }
}
